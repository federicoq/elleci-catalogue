<?

$this->view('header', $this->title);

?>
<p>See the source at: <code><a href="<? echo createUrl($this->base . 'GetSource')[0]; ?>">/app/dev/_/dev_validation.ctrl.php</a></code></p>
<?

echo '<ul>';
echo '<li><a href="'.createUrl($this->base . 'GetExample1')[0].'">Example 1 (Simple validation)</a></li>';
echo '<li><a href="'.createUrl($this->base . 'GetExample2')[0].'">Example 2 (Validation with method)</a></li>';
echo '<li><a href="'.createUrl($this->base . 'GetExample3')[0].'">Example 3 (Validation with filter)</a></li>';
echo '<li><a href="'.createUrl($this->base . 'GetExample4')[0].'">Example 4 (Validation binded to database)</a></li>';
echo '</ul>';
?>

<?
$this->view('partial-footer');
?>