<?
echo '<h3>'.$app_data["title"].'</h3>';
echo '<ul>';
foreach($app_data["menu"] as $ctrl => $section) {
	if(!is_array($section))
		echo '<li><a href="'.createUrl($this->base . $ctrl)[0].'">'.$section.'</a></li>';
	else {
		$base = $this->base . $ctrl . '/';
		echo '<li>';
		echo '<a href="'.createUrl($this->base . $ctrl)[0].'">'.$section['title'].'</a>';
		echo '<ul>';
			foreach($section["paragraphs"] as $ct => $paragraph)
				echo '<li><a href="'.createUrl($base . $ct)[0].'">'.$paragraph.'</a></li>';
		echo '</ul>';	
		echo '</li>';
	}
}
echo '</ul>';
?>