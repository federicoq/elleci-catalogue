<?

class dev_validationCtrl extends standardController {

	function __construct() {

		$this->base = 'dev/validation/';
		$this->title = 'Forms Validation';

		$this->gyuFilter(
			'before', 
			function() {
				return $this->search_validation_fail($_POST);
			}, 
			1, 
			$this->base . 'GetExample3', 
			[
				'PostExample3'
			]
		);

	}

	/*
		Example 1. Without reusable filter
	*/

	function GetExample1() {

		$this->title = 'Check if is a valid username';
		$this->action = 'Search';

		$this->view('example-form');

	}

	function PostExample1() {

		$validation = new \Gyu\Validation($_POST);
		$validation->set_rules('name', 'Username', 'required|min_length[5]|valid_email', ['required' => 'You\'ll have to insert a name!', 'min_length' => 'At least 5 chars']);

		if(!$validation->run()) {
			$this->move($this->base . 'GetExample1', ['errors' => $validation->error_array(), 'content' => $validation->validation_data]);
		} else {
			$this->view('example-in');
		}

	}

	/*
		Example 2. With reusable filter
	*/

	function GetExample2() {

		$this->title = 'Check if is a valid username';
		$this->action = 'Search';

		$this->view('example-form');

	}

	function PostExample2() {

		if($res = $this->search_validation_fail($_POST)) {
			$this->move($this->base . 'GetExample2', $res);
		} else {
			$this->view('example-in');
		}

	}

	/*
		Example 3. With gyuFilter(s)
	*/

	function GetExample3() {

		$this->title = 'Check if is a valid username';
		$this->action = 'Search';

		$this->view('example-form');

	}

	function PostExample3() {

		$this->view('example-in');
	}

	function search_validation_fail($source) {

		$validation = new \Gyu\Validation($source);
		
		$validation->set_rules('name', 'Username', 'required|min_length[10]|valid_email', ['required' => 'You\'ll have to insert a name!', 'min_length' => 'At least 10 chars']);

		if($validation->run()) {
			$this->validated = $validation->validation_data;
			return false;
		} else {
			return [
				'errors' => $validation->error_array(), 
				'content' => $validation->validation_data,
				'validation' => $validation
			];
		}

	}

	/*
		Example 4
	*/

	function GetExample4() {

		$this->title = 'This example check if the usere is already in.';
		$this->action = 'Check if exists';
		$this->view('example-form');

	}

	function PostExample4() {

		$validation = new \Gyu\Validation($_POST);
		$validation->set_rules('name', 'Username', 'required|min_length[5]|valid_email|is_unique[users.username]', ['is_unique' => 'User already in!', 'required' => 'You\'ll have to insert a name!', 'min_length' => 'At least 5 chars']);

		if(!$validation->run()) {
			$this->move($this->base . 'GetExample4', ['errors' => $validation->error_array(), 'content' => $validation->validation_data]);
		} else {
			echo 'in';
		}

	}

	/* Documentation Assets */

	function GetIndex() {

		$this->view('index');

	}

	function GetSource() {

		$this->view('header', $this->title);
		highlight_file(__FILE__);

	}

	function GetFilters() {

		$filters = [
			['rule' => 'required','parameter' => 'No','description' => 'Returns FALSE if the form element is empty.','example' => ' '],
			['rule' => 'matches','parameter' => 'Yes','description' => 'Returns FALSE if the form element does not match the one in the parameter.','example' => 'matches[form_item]'],
			['rule' => 'is_unique','parameter' => 'Yes','description' => 'Returns FALSE if the form element is not unique to the lib and field name in the parameter.','example' => 'is_unique[lib.field]'],
			['rule' => 'min_length','parameter' => 'Yes','description' => 'Returns FALSE if the form element is shorter then the parameter value.','example' => 'min_length[6]'],
			['rule' => 'max_length','parameter' => 'Yes','description' => 'Returns FALSE if the form element is longer then the parameter value.','example' => 'max_length[12]'],
			['rule' => 'exact_length','parameter' => 'Yes','description' => 'Returns FALSE if the form element is not exactly the parameter value.','example' => 'exact_length[8]'],
			['rule' => 'greater_than','parameter' => 'Yes','description' => 'Returns FALSE if the form element is less than the parameter value or not numeric.','example' => 'greater_than[8]'],
			['rule' => 'less_than','parameter' => 'Yes','description' => 'Returns FALSE if the form element is greater than the parameter value or not numeric.','example' => 'less_than[8]'],
			['rule' => 'alpha','parameter' => 'No','description' => 'Returns FALSE if the form element contains anything other than alphabetical characters.','example' => ' '],
			['rule' => 'alpha_numeric','parameter' => 'No','description' => 'Returns FALSE if the form element contains anything other than alpha-numeric characters.','example' => ' '],
			['rule' => 'alpha_dash','parameter' => 'No','description' => 'Returns FALSE if the form element contains anything other than alpha-numeric characters, underscores or dashes.','example' => ' '],
			['rule' => 'numeric','parameter' => 'No','description' => 'Returns FALSE if the form element contains anything other than numeric characters.','example' => ' '],
			['rule' => 'integer','parameter' => 'No','description' => 'Returns FALSE if the form element contains anything other than an integer.','example' => ' '],
			['rule' => 'decimal','parameter' => 'No','description' => 'Returns FALSE if the form element contains anything other than a decimal number.','example' => ' '],
			['rule' => 'is_natural','parameter' => 'No','description' => 'Returns FALSE if the form element contains anything other than a natural number: 0, 1, 2, 3, etc.','example' => ' '],
			['rule' => 'is_natural_no_zero','parameter' => 'No','description' => 'Returns FALSE if the form element contains anything other than a natural number, but not zero: 1, 2, 3, etc.','example' => ' '],
			['rule' => 'valid_email','parameter' => 'No','description' => 'Returns FALSE if the form element does not contain a valid email address.','example' => ' '],
			['rule' => 'valid_emails','parameter' => 'No','description' => 'Returns FALSE if any value provided in a comma separated list is not a valid email.','example' => ' '],
			['rule' => 'valid_ip','parameter' => 'No','description' => 'Returns FALSE if the supplied IP is not valid. Accepts an optional parameter of "IPv4" or "IPv6" to specify an IP format.','example' => ' '],
			['rule' => 'valid_base64','parameter' => 'No','description' => 'Returns FALSE if the supplied string contains anything other than valid Base64 characters.','example' => ''],
		];

		$cli = new \Gyu\Cli;
		$this->table = $cli->draw_text_table($filters, 1);

		$this->view('filters');

	}

}
