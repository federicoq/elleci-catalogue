<?php

/*
	   __           __           _          __
	  / /_ ____ _  / /  ___ ____(_)__ _____/ /__
	 / / // /  ' \/ _ \/ -_) __/ / _ `/ __/  '_/
	/_/\_,_/_/_/_/_.__/\__/_/_/ /\_,_/\__/_/\_\
	                       |___/
 */

class gyu_lumberjack {

	var $config = array();

	function __construct() {

		// Global Configuration.
		$this->config = array(
			'dev' => false,
			'run-time-missed-string' => '',
			'run-time-missed' => true,
			'seo' => array(
				'title' => siteName,
				'description' => siteName
			),
			'scope' => array(),
			'assets' => array(
				'javascript' => array(),
				'css' => array()
			),
			'assets-runtime' => array(
				'javascript' => array(),
				'css' => array()
			),
			'theme' => 'theme'
		);

		$this->forest = $this->_forest();
		$this->_use = new stdClass();

		$this->_use('config', $this->config);

		if($this->config["dev"])
			$this->runtimeJs("console.log('NOTE: disable dev for production server.')");

	}

	////////
	// Inner

	public function _use($name, $var = null) {
		if($var != null) {
			if(isset($this->_use->$name) && $this->config["dev"])
				$this->runtimeJs('console.log("_use->'.$name.' has changed value.", '.json_encode(array('was' => $this->_use->$name, 'is' => $var)).');');
			$this->_use->$name = $var;
			return true;
		} else {
			return $this->_use->$name;
		}
	}

	private function _behaviour($string) {
		if(strstr($string, ':')) {
			list($app, $ctrl) = explode(':', $string);
			return array(
				'action' => 'ctrl',
				'app' => $app,
				'ctrl' => $ctrl,
				'exec' => function($arg, $app, $ctrl) use($app, $ctrl) { return LoadApp($app, 1)->$ctrl($arg); }
			);

		} else {
			return array(
				'action' => 'application',
				'url' => $string,
				'exec' => function($arg, $string) use ($string) { Application($string, null, $arg); }
			);
		}
	}

	private function _config($config = null) {
		if($config) $this->config = array_merge_recursive($this->config, $config);
		return $this;
	}

	private function _forest() {

		$applications = deb_installed_app();
		foreach($applications as $app) {
			if(is_file(application . $app["name"] . '/_/config.lumberjack')) {
				$file = application . $app["name"] . '/_/config.lumberjack';
				$content = json_decode(file_get_contents($file), true);
				$validConfigs[$app["name"]] = $content;
			}
		}
		return $validConfigs;
	}

	function _render($what, $data = null) {

		if(isset($data) && is_array($data)) {
			foreach($data as $k => $v)
				$this->_use($k, $v);
		}

		$file = $what . '.php';
		if(is_file($file)) {

			foreach($this->_use as $K => $v)
				$$K = $v;

			$app_data = $this;

			include $file;

		}

	}

	////////

	# # #

	//////////////
	// Base Layout

	function __top($config = null) {
		if($config) $this->_config($config);
		$this->_render($this->config["theme"] . '/_v/top');
	}

	function __bottom($config = null) {
		if($config) $this->_config($config);
		$this->_render($this->config["theme"] . '/_v/bottom');
	}

	function __header($config = null) {

		if($config) $this->_config($config);

		$this->__top();
		$this->_render($this->config["theme"] . '/_v/header');

	}

	function __footer($config = null) {
		if($config) $this->_config($config);

		$this->_render($this->config["theme"] . '/_v/footer');
		$this->__bottom();
	}

	function __partial($label, $args = null) {
		if(!strstr($label, '/'))
			$label = $this->config["theme"] . '/' . $label;
		$trav = explode('/', $label);
		$output = $this->forest;
		foreach($trav as $k=>$item) $output = $k == 0 ? $output[$item]["partials"] : $output[$item];
		if(!$output) {
			if($this->config["dev"]) {
				$this->runtimeJs('console.log("Partial NOT found: '.$label.'")');
				return false;
			}
		}
		$a = $this->_behaviour($output);
		$a["exec"]($args);
		if($this->config["dev"]) $this->runtimeJs('console.log("Partial found: '.$label.'"); console.log('.json_encode($a).');');
	}

	function __menu($label) {
		foreach($this->forest as $key => $value) {
			if(isset($value["menu"][$label])) {
				$menus[$key] = array('name' => $value["name"], 'items' => array());
				foreach($value["menu"][$label] as $k => $item) {
					if(!is_array($item)) {
						$type = $this->_behaviour($item);
						$item = $type["exec"]();
						if(is_array($item[0]))
							foreach($item as $items)
								$menus[$key]["items"][] = $items;
						else
							$menus[$key]["items"][] = $item;
					} else
						$menus[$key]["items"][] = $item;
				}
			}
		}
		return $menus;
	}

	function __($label) {
		$trav = explode("/", $label);
		$output = $this->forest;
		foreach($trav as $i=>$k) $output = $i == 0 ? $output[$k]["labels"] : $output[$k];
		return $output;
	}

	//////////////

	# # #

	/////////////////////
	// Assets Information

	function ___javascripts() {

		$fileName = strtoupper(md5(serialize(array($this->config["assets"]["javascript"], $this->config["assets-runtime"]["javascript"]))));
		$outPutName = $fileName . '.js';

		if(!is_file(cache . $outPutName) || $this->config["dev"]) {

			$info = $this->___assets($this->config["assets"]["javascript"]);
			$tpm = $info[2]; ##CallFunction('gyu_optimization', 'stripComments', $info[2]);

			if($info[1]) {
				foreach($info[1] as $notFound)
					if($this->config["dev"]) $output = "console.log('JS NOT Found: ".$notFound."');\n". $tpm;
			} else
				$output = $tpm;

			if(count($this->config["assets-runtime"]["javascript"]) > 0) {
				$output .= "/* dyn.inject */\n\n" . implode("\n", $this->config["assets-runtime"]["javascript"]);
			}

			#$output = $output;

			file_put_contents(cache . $outPutName, $output);

		}

		$this->config["assets"]["javascript"] = array();
		$this->config["assets-runtime"]["javascript"] = array();
		$this->config["run-time-missed"] = true;

		return('/cdn/cache/' . $outPutName);

	}

	function ___css() {

		$fileName = strtoupper(md5(serialize(array($this->config["assets"]["css"], $this->config["assets-runtime"]["css"]))));
		$outPutName = $fileName . '.css';

		if(!is_file(cache . $outPutName) || $this->config["dev"]) {

			$info = $this->___assets($this->config["assets"]["css"]);
			$less = LoadClass('lessc', 1);
			$tpm = $less->compile($info[2]);

			if($info[1]) {
				foreach($info[1] as $notFound) {
					$output = "/* CSS NOT Found: ".$notFound." */\n". $tpm;
					if($this->config["dev"]) $this->runtimeJs('console.log("CSS NOT Found: '.$notFound.'")');
				}
			} else
				$output = $tpm;

			if(count($this->config["assets-runtime"]["css"]) > 0)
				$output .= implode("\n", $this->config["assets-runtime"]["css"]);

			#$output = "/*\n".file_get_contents(application . 'gyu_lumberjack/_/ascii.txt') . "\n\n".date('r')."\n\n*/\n\n" . $output;

			file_put_contents(cache . $outPutName, $output);

		}

		$this->config["assets"]["css"] = array();
		$this->config["assets-runtime"]["css"] = array();

		$this->config["run-time-missed"] = true;

		return('/cdn/cache/' . $outPutName);

	}

	function runtimeCss($rule) {
		$this->runtimeAdd('css', $rule);
	}

	function runtimeJs($rule) {
		$this->runtimeAdd('javascript', $rule);
	}

	function runtimeAdd($type, $rule) {
		array_push($this->config["assets-runtime"][$type], $rule);
	}

	function ___runtimeMissed() {

		if(count($this->config["assets"]["javascript"]) > 0 || count($this->config["assets-runtime"]["javascript"]) > 0)
			$output .= '<script src="'.$this->___javascripts().'"></script>' . "\n";
		if(count($this->config["assets"]["css"]) > 0 || count($this->config["assets-runtime"]["css"]) > 0)
			$output .= '<link rel="stylesheet" href="'.$this->___css().'" />' . "\n";

		if(strlen($output) > 0)
			$output = "\n" . '<!-- ' . "\n" .file_get_contents(application . 'gyu_lumberjack/_/ascii.txt'). "\n" . ' -->' . "\n" . $output;

		return $output;

	}

	function ___assets($files) {
		$output = '';
		foreach($files as $file) {
			if(is_file($file)) {
				$goodFiles[] = $file;
				$output .= file_get_contents($file);
			} else if(strstr($file, 'http://')) {
				$goodFiles[] = $file;
				$output .= file_get_contents($file);
			} else {
				$badFiles[] = $file;
			}
		}
		return array($goodFiles, $badFiles, $output);
	}

	function addJavascript($url) {
		return $this->addAsset('javascript', $url);
	}

	function inJavascript($what) {
		return $this->inAsset('javascript', $what);
	}

	function addCss($url) {
		return $this->addAsset('css', $url);
	}

	function inCss($what) {
		return $this->inAsset('css', $what);
	}

	function inAsset($type, $what) {
		if(in_array($what, $this->config["assets"][$type]))
			return true;
		return false;
	}

	function addAsset($type, $url) {
		if(!$this->inAsset($type, $url))
			array_push($this->config["assets"][$type], $url);
	}

	/////////////////////

	# # #

	/////////
	// Scopes

	/*
	 * Add a string to the scope.
	 */

	function addScope($what) {
		if(!$this->inScope($what)) {
			array_push($this->config["scope"], $what);
		}
		return $this;
	}

	/*
	 * Check if a given string is in the scope.
	 */

	function inScope($what) {
		if(in_array($what, $this->config["scope"]))
			return true;
		return false;
	}
	/////////

	# # #

	///////////
	// Examples
/*
	function GetTest() {

		$ui = LoadClass('gyu_lumberjack', 1);

		$ui->addScope('home');

		// Javascript + Css // Like assets
		$ui->addJavascript('cdn/javascripts/jquery-1.10.2.js');
		$ui->addJavascript('cdn/javascripts/app.js');

		$ui->addCss('cdn/css/style.less');

		// Or custom config
		$c["assets"]["javascript"][] = 'cdn/javascripts/jquery.js';

		// Javascript + Css // Like injection.

		$ui->runtimeCss('.title { width: 300px }');
		$ui->runtimeJs('$(function() { console.log("wof") });');

		$ui->__header($c);

		$ui->__partial('menu', $ui->__menu('header'));

		## For pass vars to the view.. then use: $app_data->_use('name'); or: $app_data->_use->name;
		$ui->_use('name', 'Federico Quagliotto');

		Application('users/_v/test2', null, $ui);
		
		#Application('users/_v/test', null, $ui);
		echo '<pre>' .
			print_r($ui->__partial('users/vCard', 'federico'), 1) .
			print_r($ui->__partial('users/layout', 'federico'), 1) .
			print_r($ui->__menu('header'), 1) .
			print_r($ui, 1) .
		'</pre>';

#		$ui->runtimeCss('h1 { color: yellow }');
#		$ui->runtimeJs('$(function() { alert("wof") });');

		$ui->__footer();

	}*/

	///////////

}