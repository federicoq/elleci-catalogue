<?php

/**
 * Api Functions
 * 
 * @package Api
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

function api__to_xml(SimpleXMLElement $object, $data) {
	if(is_object($data))
		$data = (array) $data;

	foreach ($data as $key => $value) {
		if(is_object($value))
			$value = (array) $value;

		if(is_numeric($key))
			$key = 'item_' . $key;

		if(is_array($value)) {
			$new_object = $object->addChild($key);
			CallFunction('api', 'to_xml', $new_object, $value);
		} else {
			$object->addChild($key, $value);
		}
	}
}
